"use strict";

const pubnub = require('./config/config');

pubnub.subscribe({
	channel: 'optimiza_chile',
    message : function (m) {
		console.log(m.title);
		console.log(m.text.replace(/[∙%$&/(]+/g, ''));
		process.exit(0);
    },
    error : function (error) {
		console.log(JSON.stringify(error));
		process.exit(1);
    },
});
