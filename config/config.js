"use strict";

const PN = require('pubnub');

var pubnub = PN.init({
	publish_key: 'pub-c-7f32b1c9-5d92-4d99-8976-19999859d12a',
	subscribe_key: 'sub-c-fa59df94-31aa-11e6-be83-0619f8945a4f',
	error: function (error) {
		console.log(`Error: ${error}`);
	},
});

module.exports = pubnub;
